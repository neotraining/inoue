﻿namespace MoveCircle
{
    partial class FormBallGame
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージド リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.selectpictureBox = new System.Windows.Forms.PictureBox();
            this.textTimer = new System.Windows.Forms.TextBox();
            this.restartButton = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.textHunt = new System.Windows.Forms.TextBox();
            this.mainpictureBox = new System.Windows.Forms.PictureBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.selectpictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainpictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.selectpictureBox);
            this.splitContainer1.Panel1.Controls.Add(this.textTimer);
            this.splitContainer1.Panel1.Controls.Add(this.restartButton);
            this.splitContainer1.Panel1.Controls.Add(this.label5);
            this.splitContainer1.Panel1.Controls.Add(this.label4);
            this.splitContainer1.Panel1.Controls.Add(this.label3);
            this.splitContainer1.Panel1.Controls.Add(this.label2);
            this.splitContainer1.Panel1.Controls.Add(this.label1);
            this.splitContainer1.Panel1.Controls.Add(this.textHunt);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.mainpictureBox);
            this.splitContainer1.Size = new System.Drawing.Size(1034, 761);
            this.splitContainer1.SplitterDistance = 70;
            this.splitContainer1.TabIndex = 0;
            // 
            // selectpictureBox
            // 
            this.selectpictureBox.BackColor = System.Drawing.Color.White;
            this.selectpictureBox.Location = new System.Drawing.Point(396, 9);
            this.selectpictureBox.Name = "selectpictureBox";
            this.selectpictureBox.Size = new System.Drawing.Size(250, 50);
            this.selectpictureBox.TabIndex = 10;
            this.selectpictureBox.TabStop = false;
            this.selectpictureBox.MouseClick += new System.Windows.Forms.MouseEventHandler(this.selectPictureBox_MouseClick);
            // 
            // textTimer
            // 
            this.textTimer.Font = new System.Drawing.Font("Meiryo UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textTimer.Location = new System.Drawing.Point(840, 12);
            this.textTimer.Multiline = true;
            this.textTimer.Name = "textTimer";
            this.textTimer.Size = new System.Drawing.Size(129, 47);
            this.textTimer.TabIndex = 9;
            this.textTimer.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // restartButton
            // 
            this.restartButton.Location = new System.Drawing.Point(692, 18);
            this.restartButton.Name = "restartButton";
            this.restartButton.Size = new System.Drawing.Size(82, 35);
            this.restartButton.TabIndex = 8;
            this.restartButton.Text = "再スタート";
            this.restartButton.UseVisualStyleBackColor = true;
            this.restartButton.Click += new System.EventHandler(this.restartButton_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(991, 29);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(17, 12);
            this.label5.TabIndex = 6;
            this.label5.Text = "秒";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(115, 49);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(261, 12);
            this.label4.TabIndex = 5;
            this.label4.Text = "↓下のエリアをクリックするとボールの位置が変わります。";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(790, 29);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 12);
            this.label3.TabIndex = 4;
            this.label3.Text = "記録：";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(140, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(250, 12);
            this.label2.TabIndex = 3;
            this.label2.Text = "下の背景に表示された漢字と同じ色の円をクリック→";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(63, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(36, 12);
            this.label1.TabIndex = 2;
            this.label1.Text = "を探せ";
            // 
            // textHunt
            // 
            this.textHunt.Font = new System.Drawing.Font("Meiryo UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textHunt.Location = new System.Drawing.Point(12, 14);
            this.textHunt.Multiline = true;
            this.textHunt.Name = "textHunt";
            this.textHunt.Size = new System.Drawing.Size(45, 47);
            this.textHunt.TabIndex = 0;
            // 
            // mainpictureBox
            // 
            this.mainpictureBox.BackColor = System.Drawing.Color.White;
            this.mainpictureBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainpictureBox.Location = new System.Drawing.Point(0, 0);
            this.mainpictureBox.Name = "mainpictureBox";
            this.mainpictureBox.Size = new System.Drawing.Size(1034, 687);
            this.mainpictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.mainpictureBox.TabIndex = 0;
            this.mainpictureBox.TabStop = false;
            this.mainpictureBox.MouseClick += new System.Windows.Forms.MouseEventHandler(this.mainPictureBox_MouseClick);
            // 
            // timer1
            // 
            this.timer1.Interval = 20;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // FormBallGame
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1034, 761);
            this.Controls.Add(this.splitContainer1);
            this.Name = "FormBallGame";
            this.Text = "間違いボール探し";
            this.Load += new System.EventHandler(this.FormBallGame_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.selectpictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainpictureBox)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textHunt;
        private System.Windows.Forms.TextBox textTimer;
        private System.Windows.Forms.Button restartButton;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.PictureBox mainpictureBox;
        private System.Windows.Forms.PictureBox selectpictureBox;
    }
}

