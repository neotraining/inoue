﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace MoveCircle
{
    public partial class FormBallGame : Form
    {
        private Bitmap canvas;
        //private Ball balls1;
        //private Ball balls2;
        //private Ball balls3;
        //private Ball balls4;
        //private Ball balls5;


        private Ball[] balls;
        private int ballCount = 5;
        private string[] kanjis;
        private Brush[] brushes;
        private string fontName = "HG教科書体";
        private string correctText = "荻";
        private string mistakeText = "萩";
        private string circleText = "〇";
        private double nowTime = 0;
     
        private int randomResult = 0;

        public FormBallGame()
        {
            InitializeComponent();
        }

        private void DrowMainPictureBox(Brush color, string text, bool trueFlag)
        {
            int height = mainpictureBox.Height;
            int width = mainpictureBox.Width;

            if (canvas == null)
            {
                canvas = new Bitmap(width, height);
            }

            using (Graphics g = Graphics.FromImage(canvas))
            {
                if (trueFlag)
                {
                    g.FillRectangle(Brushes.LightPink, 0, 0, width, height);
                }
                else
                {
                    g.FillRectangle(Brushes.White, 0, 0, width, height);
                }
                g.DrawString(text,
                    new Font(fontName, height - height / 4), color, 0, 0, new StringFormat());

                mainpictureBox.Image = canvas;
            }
        }



        private void InitGraphics()
        {
            brushes = new Brush[ballCount];
            kanjis = new string[ballCount];
            balls = new Ball[ballCount];

            brushes[0] = Brushes.LightPink;
            brushes[1] = Brushes.LightBlue;
            brushes[2] = Brushes.LightGray;
            brushes[3] = Brushes.LightCoral;
            brushes[4] = Brushes.LightGreen;


            DrowCircleSelectPictureBox();

            DrowMainPictureBox(Brushes.Gray, correctText, false);

            restartButton.Enabled = false;
            textHunt.Text = correctText;
        }

        private void SetStartPosition()
        {
            for (int i = 0; i < ballCount; i++)
            {
                kanjis[i] = mistakeText;
            }

            randomResult = new Random().Next(ballCount);
            kanjis[randomResult] = correctText;

            for (int i = 0; i < ballCount; i++)
            {
                balls[i] = new Ball(mainpictureBox, canvas, brushes[i], kanjis[i]);

                //int rndXMax = mainpictureBox.Width;
                //int rndYMax = mainpictureBox.Height;
                //SetBalls(new Random().Next(rndXMax), new Random().Next(rndYMax));

                nowTime = 0;
                timer1.Start();
            }

            int rndXMax = mainpictureBox.Width;
            int rndYMax = mainpictureBox.Height;
            SetBalls(new Random().Next(rndXMax), new Random().Next(rndYMax));

        }

        private void SetBalls(int x, int y)
        {
            int rndXMax = mainpictureBox.Width;
            int rndYMax = mainpictureBox.Height;
            int rndX;
            int rndY;


            for (int i = 0; i < ballCount; i++)
            {
                rndX = new Random(i * x).Next(rndXMax);
                rndY = new Random(i * y).Next(rndYMax);

                balls[i].DeleteCircle();
                balls[i].putCircle(rndX, rndY);
            }
        }

        private void FormBallGame_Load(object sender, EventArgs e)
        {

            InitGraphics();
            SetStartPosition();

            #region
            //    DrowCircleSelectPictureBox();
            //    DrowMainPictureBox(Brushes.Gray, correctText);
            //    textHunt.Text = correctText;

            //    balls = new Ball[ballCount];
            //    for (int i = 0; i < ballCount; i++)
            //    {
            //        balls[i] = new Ball(mainpictureBox, canvas, brushes[i], kanjis[i]);
            //    }
            //    for (int i = 0; i < balls.Length; i++)
            //    {
            //        balls[i].Move();
            //    }
            //  
            //    balls[0].putCircle(100, 100);
            //    balls[1].putCircle(
            //  300, 100);
            //    balls[2].putCircle(300, 300);
            //    balls[3].putCircle(100, 300);
            //    balls[4].putCircle(500, 500);

            //    timer1.Start();
            //}
        }
      
        private void DrowCircleSelectPictureBox()
        {
            int height = selectpictureBox.Height;
            int width = selectpictureBox.Width;

            Bitmap selectCanvas = new Bitmap(width, height);
            using (Graphics g = Graphics.FromImage(selectCanvas))
            {
                //g.FillEllipse(Brushes.LightBlue, 0, 0, height, height);

                for (int i = 0; i < ballCount; i++)
                {
                    g.FillEllipse(brushes[i], i * height, 0, height, height);
                }
                selectpictureBox.Image = selectCanvas;
            }
        }


            #endregion

            private void restartButton_Click(object sender, EventArgs e)
            {
                InitGraphics();
                SetStartPosition();
            }

            private void selectPictureBox_MouseClick(object sender, MouseEventArgs e)
            {
                if (restartButton.Enabled)
                {
                    return;
                }

                if (e.Button == MouseButtons.Left)
                {
                    int selectCircle = e.X / selectpictureBox.Height;
                    if (randomResult == selectCircle)
                    {
                        timer1.Stop();
                        DrowMainPictureBox(Brushes.Red, circleText, true);
                        restartButton.Enabled = true;
                    }
                    else
                    {
                        DrowMainPictureBox(Brushes.Red, correctText, false);

                        for (int i = 0; i < ballCount; i++)
                        {
                            balls[i].pitch = balls[i].pitch - balls[i].pitch / 2;
                        }
                        nowTime = nowTime + 10;
                    }
                }
            }

            private void mainPictureBox_MouseClick(object sender, MouseEventArgs e)
            {
                if (restartButton.Enabled)
                {
                    return;
                }

                SetBalls(e.X, e.Y);
            }

            private void timer1_Tick(object sender, EventArgs e)
            {
                for (int i = 0; i < ballCount; i++)
                {
                    balls[i].Move();
                }
                //balls[0].Move();
                //balls[1].Move();
                //balls[2].Move();
                //balls[3].Move();
                //balls[4].Move();

                nowTime = nowTime + 0.02;
                textTimer.Text = nowTime.ToString("0.00");
            }
        }
    }