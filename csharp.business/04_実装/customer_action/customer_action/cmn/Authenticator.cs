﻿using System;

namespace customer_action.cmn
{
    public class Authenticator : System.Web.UI.Page
    {
        public Authenticator()
        {

        }

        protected void Page_Load(object sender, EventArgs e)
        {

            if (Utilities.IsLogon(Session[Constants.AdminFlag]?.ToString()))
            {

            }
            else
            {
                // 認証されていない場合の処理
                // ログオン画面に遷移する
                Response.Redirect("Logon.aspx");
            }
        }

    }
}