﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;

namespace customer_action.cmn
{
    public static class Utilities
    {
        /// <summary>
        /// 認証済みか否か検証します
        /// </summary>
        /// <returns>
        /// true ：認証済み
        /// false：認証されていない
        /// </returns>
        public static bool IsLogon(string adminFlag)
        {
            return adminFlag == null ? false : true;
        }

    }
}