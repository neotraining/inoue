﻿namespace customer_action.cmn

{
    public class Constants
    {
        public static readonly string StaffID = "StaffID";
        public static readonly string StaffName = "StaffName";
        public static readonly string AdminFlag = "AdminFlag";
    }
}