﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="CustomerEntry.aspx.cs" Inherits="customer_action.CustomerEntry" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style2 {
            height: 23px;
        }
        .auto-style3 {
            height: 16px;
            width: 371px;
        }
        .auto-style4 {
            width: 226px;
        }
        .auto-style5 {
            height: 16px;
            width: 226px;
        }
        .auto-style6 {
            height: 16px;
            width: 118px;
        }
        .auto-style7 {
            width: 118px;
        }
        .auto-style10 {
            width: 371px;
        }
        .auto-style11 {
            height: 16px;
        }
        .auto-style12 {
            width: 118px;
            height: 23px;
        }
        .auto-style14 {
            width: 50px;
        }
        .auto-style15 {
            width: 159px;
        }
        .auto-style16 {
            height: 12px;
        }
        .auto-style17 {
            width: 949px;
        }
        .auto-style18 {
            width: 118px;
            height: 22px;
        }
        .auto-style19 {
            height: 22px;
        }
        .auto-style20 {
            margin-bottom: 0px;
        }
        .auto-style21 {
            width: 800px;
            margin-right: 0px;
        }
        .auto-style22 {
            height: 16px;
            width: 94px;
        }
        .auto-style23 {
            width: 94px;
        }
        .auto-style24 {
            width: 226px;
            height: 22px;
        }
        .auto-style25 {
            width: 94px;
            height: 22px;
        }
        .auto-style26 {
            width: 371px;
            height: 22px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <p class="auto-style16">
        <table class="auto-style17">
            <tr>
                <td>
                    <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Size="Large" Text="新規顧客登録"></asp:Label>
                </td>
                <td>&nbsp;</td>
                <td>
                    <asp:Button ID="returnButton" runat="server" OnClick="returnButton_Click" Text="メニューへ戻る" />
                </td>
            </tr>
        </table>
        <table class="auto-style21">
            <tr>
                <td class="auto-style6">顧客ID</td>
                <td class="auto-style5">
                    <asp:Label ID="NewCustomerID" runat="server"></asp:Label>
                </td>
                <td class="auto-style22">営業担当者</td>
                <td class="auto-style3" colspan="2">
                    <asp:DropDownList ID="DropDownList1" runat="server" AppendDataBoundItems="True" DataSourceID="SqlDataSource1" DataTextField="staff_name" DataValueField="staffID">
                        <asp:ListItem Value="-1">（選択してください）</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="auto-style7">顧客名カナ</td>
                <td class="auto-style4">
                    <asp:TextBox ID="Customer_kanaTxt" runat="server"></asp:TextBox>
                </td>
                <td class="auto-style23">初回訪問日</td>
                <td class="auto-style10">
                    <asp:TextBox ID="firstActionTxt" runat="server" Width="191px" OnTextChanged="firstActionTxt_TextChanged" AutoPostBack="True" CssClass="auto-style20"></asp:TextBox>
                </td>
                <td class="auto-style10" rowspan="5">
                    <asp:Calendar ID="Calendar1" runat="server" NextPrevFormat="ShortMonth" OnSelectionChanged="Calendar1_SelectionChanged" SelectedDate="03/28/2019 14:27:01"></asp:Calendar>
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style18">顧客名</td>
                <td colspan="2" class="auto-style19">
                    <asp:TextBox ID="CustomerNameTxt" runat="server" Width="276px"></asp:TextBox>
                </td>
                <td class="auto-style19">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style7">会社名</td>
                <td class="auto-style4">
                    <asp:DropDownList ID="DropDownList2" runat="server" AppendDataBoundItems="True" DataSourceID="SqlDataSource2" DataTextField="company_name" DataValueField="companyID">
                        <asp:ListItem Value="-1">(選択してください)</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td class="auto-style23">&nbsp;</td>
                <td class="auto-style10">&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style18">部署</td>
                <td class="auto-style24">
                    <asp:TextBox ID="Sectiontxt" runat="server" Width="199px"></asp:TextBox>
                </td>
                <td class="auto-style25">役職</td>
                <td class="auto-style26">
                    <asp:TextBox ID="PositionTxt" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style12">郵便番号</td>
                <td colspan="2" class="auto-style2">
                    <asp:TextBox ID="ZipTxt" runat="server" Width="99px"></asp:TextBox>
                </td>
                <td class="auto-style2">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style6">住所</td>
                <td class="auto-style11" colspan="4">
                    <asp:TextBox ID="AddressTxt" runat="server" Width="523px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style6">TEL</td>
                <td class="auto-style11" colspan="4">
                    <asp:TextBox ID="TelTxt" runat="server" Width="203px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style6">備考</td>
                <td class="auto-style11" colspan="4">
                    <asp:TextBox ID="MemoTxt" runat="server" Height="37px" Width="517px"></asp:TextBox>
                </td>
            </tr>
        </table>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:customer_actionConnectionString2 %>" OnSelecting="SqlDataSource1_Selecting" SelectCommand="SELECT * FROM [tbl_staff]"></asp:SqlDataSource>
        <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:customer_actionConnectionString2 %>" SelectCommand="SELECT * FROM [tbl_company]"></asp:SqlDataSource>
    
        <table class="auto-style1">
            <tr>
                <td>&nbsp;</td>
                <td class="auto-style15">
                    <asp:Button ID="RegisterButton" runat="server" OnClick="RegisterButton_Click" Text="登録" Width="98px" />
                </td>
                <td aria-checked="false" class="auto-style14">&nbsp;</td>
                <td>
                    <asp:Button ID="ClearButton" runat="server" Text="キャンセル" Width="93px" OnClick="ClearButton_Click" />
                </td>
                <td>&nbsp;</td>
            </tr>
        </table>
    
    </p>
    <p>
        &nbsp;</p>
    </asp:Content>
