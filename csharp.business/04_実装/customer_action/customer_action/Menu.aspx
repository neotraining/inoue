﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="Menu.aspx.cs" Inherits="customer_action.Menu" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style2 {
            width: 71px;
            height: 25px;
        }
        .auto-style3 {
            height: 24px;
        }
        .auto-style4 {
            width: 71px;
            height: 24px;
        }
        .auto-style5 {
            height: 23px;
        }
        .auto-style6 {
            width: 71px;
            height: 23px;
        }
    .auto-style7 {
        height: 22px;
            width: 632px;
        }
    .auto-style8 {
        width: 71px;
        height: 22px;
    }
        .auto-style9 {
            width: 632px;
            height: 25px;
        }
        .auto-style10 {
            height: 24px;
            width: 632px;
        }
        .auto-style11 {
            height: 121px;
            width: 664px;
        }
        .auto-style12 {
            width: 667px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Panel ID="UserPanel" runat="server">
        <table id="Table1" runat="server" height="174px" class="auto-style12">

            <tr>
                <td class="auto-style3"><h3>メニュー</h3></td>
                <td class="auto-style4">ログオフ</td>
            </tr>
            <tr>
                <td class="auto-style5">
                    <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/CustomerList.aspx">顧客一覧</asp:HyperLink>
                </td>
                <td class="auto-style6"></td>
            </tr>
            <tr>
                <td class="auto-style5">
                    <asp:HyperLink ID="HyperLink2" runat="server">営業報告一覧</asp:HyperLink>
                </td>
                <td class="auto-style6"></td>
            </tr>
            <tr>
                <td class="auto-style3">
                    <asp:HyperLink ID="HyperLink3" runat="server">会社マスター管理</asp:HyperLink>
                </td>
                <td class="auto-style4"></td>
            </tr>
            
  
        </table>
    </asp:Panel>
    <asp:Panel ID="AdminPanel" runat="server">
          <table id="Table2" runat="server" class="auto-style11">

           <tr>
                <td class="auto-style7">
                    <asp:HyperLink ID="HyperLink7" runat="server">顧客データのエクスポート</asp:HyperLink>
                </td>
                <td class="auto-style8"></td>
            </tr>
            <tr>
                <td class="auto-style9">
                    <asp:HyperLink ID="HyperLink8" runat="server" NavigateUrl="~/StaffReplace.aspx">営業担当者の置換</asp:HyperLink>
                </td>
                <td class="auto-style2"></td>
            </tr>
            <tr>
                <td class="auto-style10">
                    <asp:HyperLink ID="HyperLink9" runat="server">スタッフマスター管理</asp:HyperLink>
                </td>
                <td class="auto-style4"></td>
                </tr>
        </table>
    </asp:Panel>
</asp:Content>