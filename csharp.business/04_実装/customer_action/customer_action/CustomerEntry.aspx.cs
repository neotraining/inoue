﻿using System;
using System.Data.SqlClient;
using System.Text;
using System.Web.UI.WebControls;

namespace customer_action
{
    public partial class CustomerEntry : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        protected void SqlDataSource1_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
        {
        }

        protected void TextBox1_TextChanged(object sender, EventArgs e)
        {
        }
        protected void Calendar1_SelectionChanged(object sender, EventArgs e)
        {
            firstActionTxt.Text = Calendar1.SelectedDate.ToShortDateString();
        }

        protected void RegisterButton_Click(object sender, EventArgs e)
        {
            //接続文字列を取得する
            string connectionString = System.Configuration.ConfigurationManager.
                ConnectionStrings["customer_actionConnectionString2"].ConnectionString;

            int customerID;
            //コネクションを定義する
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                //コネクションを開く
                connection.Open();

                var query = new StringBuilder();
                query.AppendLine(" SELECT max(customerID)");
                query.AppendLine("   FROM tbl_customer");

                //コマンドを定義する
                SqlCommand command2 = new SqlCommand(query.ToString(), connection);

                SqlDataReader reader = command2.ExecuteReader();

                reader.Read();
                customerID = int.Parse(reader[0].ToString()) + 1;
                //コマンドにSQLステートメントを指定する
            }

            //           string connectionString2 = System.Configuration.ConfigurationManager.
            //ConnectionStrings["customer_actionConnectionString2"].ConnectionString;

            //コネクションを定義する
            using (SqlConnection connection2 = new SqlConnection(connectionString))
            {
                var sb = new StringBuilder();
                sb.Append("INSERT ");
                sb.Append("INTO [tbl_customer] ( ");
                sb.Append("  [customerID] ");
                sb.Append("  , [customer_name] ");
                sb.Append("  , [customer_kana] ");
                sb.Append("  , [companyID] ");
                sb.Append("  , [section] ");
                sb.Append("  , [post] ");
                sb.Append("  , [zipcode] ");
                sb.Append("  , [address] ");
                sb.Append("  , [tel] ");
                sb.Append("  , [staffID] ");
                sb.Append("  , [first_action_date] ");
                sb.Append("  , [memo] ");

                sb.Append(") ");
                sb.Append("VALUES ( ");
                sb.Append("  @customerID ");
                sb.Append("  , @customer_name ");
                sb.Append("  , @customer_kana ");
                sb.Append("  , @companyID ");
                sb.Append("  , @section ");
                sb.Append("  , @post ");
                sb.Append("  , @zipcode ");
                sb.Append("  , @address ");
                sb.Append("  , @tel ");
                sb.Append("  , @staffID ");
                sb.Append("  , @first_action_date ");
                sb.Append("  , @memo ");
                sb.Append(") ");

                SqlCommand command = connection2.CreateCommand();
                //コネクション2を開く
                connection2.Open();
                command.CommandText = sb.ToString();

                command.Parameters.Add(new SqlParameter("@customerID", customerID));
                command.Parameters.Add(new SqlParameter("@customer_name", CustomerNameTxt.Text));
                command.Parameters.Add(new SqlParameter("@customer_kana", Customer_kanaTxt.Text));
                //command.Parameters.Add(new SqlParameter("@companyID", 999));
                command.Parameters.Add(new SqlParameter("@companyID", DropDownList2.SelectedValue));
                command.Parameters.Add(new SqlParameter("@section", Sectiontxt.Text));
                command.Parameters.Add(new SqlParameter("@post", PositionTxt.Text));
                command.Parameters.Add(new SqlParameter("@zipcode", ZipTxt.Text));
                command.Parameters.Add(new SqlParameter("@address", AddressTxt.Text));
                command.Parameters.Add(new SqlParameter("@tel", TelTxt.Text));
                command.Parameters.Add(new SqlParameter("@staffID", DropDownList1.SelectedValue));
                command.Parameters.Add(new SqlParameter("@first_action_date", firstActionTxt.Text));
                command.Parameters.Add(new SqlParameter("@memo", MemoTxt.Text));

                command.ExecuteNonQuery();
            }
        }

        protected void returnButton_Click(object sender, EventArgs e)
        {
            Response.Redirect("Menu.aspx");
        }

        protected void ClearButton_Click(object sender, EventArgs e)
        {
            Response.Redirect("CustomerList.aspx");
        }

   

        protected void firstActionTxt_TextChanged(object sender, EventArgs e)
        {
          
        }

        protected void Calendar1_SelectionChanged1(object sender, EventArgs e)
        {

        }
    }
}