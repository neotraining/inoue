﻿using customer_action.cmn;
using System;

namespace customer_action
{
    public partial class Menu : Authenticator
    {
        protected new void Page_Load(object sender, EventArgs e)
        {
            if (Utilities.IsLogon(Session[Constants.AdminFlag].ToString()))
            {

            }
            else
            {
                Response.Redirect("Logon.aspx");
            }

            if (IsLogon())
            {
                // 認証済みの処理
                AdminPanel.Visible = Convert.ToBoolean(Session[Constants.AdminFlag]);

                //if (Convert.ToBoolean(Session[Constants.AdminFlag]))
                //{
                //    // 管理者の場合
                //    AdminPanel.Visible=true;
                //}
                //else
                //{
                //    AdminPanel.Visible = false;
                //}
            }
            else
            {
                // 認証されていない場合の処理
                // ログオン画面に遷移する
                Response.Redirect("Logon.aspx");
            }
        }

        /// <summary>
        /// 認証済みか否か検証します
        /// </summary>
        /// <returns>
        /// true ：認証済み
        /// false：認証されていない
        /// </returns>
        private bool IsLogon()
        {
            return Session[Constants.AdminFlag] == null ? false : true;

            //if (Session[Constants.AdminFlag] == null)
            //{
            //    return false;
            //}
            //else
            //{
            //    return true;
            //}
        }
    }
}