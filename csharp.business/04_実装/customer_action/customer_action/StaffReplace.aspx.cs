﻿using customer_action.cmn;
using System;
using System.Data.SqlClient;

namespace customer_action
{
    public partial class StaffReplace : Authenticator
    {
        private const string NormalEnd = "正常終了";

        //protected void Page_Load(object sender, EventArgs e)
        //{
            
        //    if (Utilities.IsLogon(Session[Constants.AdminFlag]?.ToString()))
        //    {
                
        //    }
        //    else
        //    {
        //        // 認証されていない場合の処理
        //        // ログオン画面に遷移する
        //        Response.Redirect("Logon.aspx");
        //    }
        //}

        protected void ExecuteButton_Click(object sender, EventArgs e)
        {
            //変更前の担当者を表す変数の定義と値のセット
            string beforeID = BeforeStaffDropDownList.SelectedValue;
            //変更後の担当者を表す変数の定義と値のセット
            string afterID = AfterStaffDropDownList.SelectedValue;

            ////変更前と変更後の担当者が選択されているかどうかを確認する
            //if(beforeID == "-1"|| afterID == "-1")
            //{
            //    //担当者が選択されていない場合には、メッセージを表示して処理を終了する
            //    MessageLabel.Text = "変更前と変更後の担当者が同じであるため、処理を実行できません。";
            //    MessageLabel.ForeColor = System.Drawing.Color.Red;
            //    return;
            //}

            ////変更前と変更後の担当者が同じであるかどうかを確認する
            //if(beforeID == afterID)
            //{
            //    //同じ担当者を選択している場合には、メッセージを表示して処理を終了する
            //    MessageLabel.Text = "変更前と変更後の担当者が同じであるため、処理を実行できません。";
            //    MessageLabel.ForeColor = System.Drawing.Color.Red;
            //    return;
            //}

            // todo: 上記エラーチェックをメソッド化する
            if (isInputError(beforeID, afterID) == true) return;

            try
            {
                //接続文字列を取得する
                string connectionString = System.Configuration.ConfigurationManager.
                    ConnectionStrings["customer_actionConnectionString2"].ConnectionString;

                //コネクションを定義する
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    //コマンドを定義する
                    SqlCommand command = connection.CreateCommand();

                    //コネクションを開く
                    connection.Open();

                    //コマンドにSQLステートメントを指定する
                    //command.CommandText = $"UPDATE tbl_customer SET staffID={ afterID  }WHERE staffID={ beforeID}";
                    command.CommandText = $"UPDATE tbl_customer SET staffID= @afterID WHERE staffID =@beforeID";

                    command.Parameters.Add(new SqlParameter("@afterID", afterID));
                    command.Parameters.Add(new SqlParameter("@beforeID", beforeID));

                    //コマンドに定義したSQLステートメントを実行して、処理結果の件数を取得する
                    int lines = command.ExecuteNonQuery();

                    //ラベルに結果のメッセージを表示する
                    MessageLabel.Text = lines + "件の顧客で担当者を変更しました。";
                }
            }
            catch (Exception ex)
            { //不明なエラーが発生したとき
                MessageLabel.Text = "エラーが発生したため、処理を中止します。<br>" + ex.Message;
            }
        }

        //

        /// <summary>
        /// 入力チェック
        /// </summary>
        /// <param name="beforeID">変更前担当者ID</param>
        /// <param name="afterID"> 変更後担当者ID</param>
        /// <returns>
        ///   エラーが存在する場合　：true
        ///   エラーが存在しない場合：false
        /// </returns>
        private bool isInputError(string beforeID, string afterID)
        {
            // エラーメッセージを初期化
            MessageLabel.Text = string.Empty;

            // 変更前と変更後の担当者が選択されているかどうかを確認する
            if (beforeID == "-1" || afterID == "-1")
            {
                MessageLabel.Text = "担当者を選択してください。";
                // MessageLabel.ForeColor = System.Drawing.Color.Red;
            }

            // 変更前と変更後の担当者が同じであるかどうかを確認する
            if (beforeID == afterID)
            {
                MessageLabel.Text = "変更前と変更後の担当者が同じであるため、処理を実行できません。";
            }

            // エラーがない場合は、正常終了のメッセージを設定する。
            if (MessageLabel.Text == "")
            {
                MessageLabel.Text = NormalEnd;
                MessageLabel.ForeColor = System.Drawing.Color.Black;
            }
            else
            {
                MessageLabel.ForeColor = System.Drawing.Color.Red;
            }
            //todo 一行で書く
            // エラーの有無を返す
            //if (MessageLabel.Text != "")
            //{
            //    return false;
            //}
            //else
            //{
            //    return true;
            //}
            return MessageLabel.Text == "正常終了" ? false : true;
            return (MessageLabel.Text != "正常終了");
        }

        /// <summary>
        /// 認証済みか否か検証します
        /// </summary>
        /// <returns>
        /// true ：認証済み
        /// false：認証されていない
        /// </returns>
        //private bool IsLogon()
        //{
        //    return Session[Constants.AdminFlag] == null ? false : true;
        //}
    }
}