﻿namespace TwitterPost
{
    partial class FormTwitterPost
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージド リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.butAuthorize = new System.Windows.Forms.Button();
            this.txtPIN = new System.Windows.Forms.TextBox();
            this.butInputPIN = new System.Windows.Forms.Button();
            this.txtTweet = new System.Windows.Forms.TextBox();
            this.butTweet = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // butAuthorize
            // 
            this.butAuthorize.Location = new System.Drawing.Point(26, 22);
            this.butAuthorize.Name = "butAuthorize";
            this.butAuthorize.Size = new System.Drawing.Size(75, 23);
            this.butAuthorize.TabIndex = 0;
            this.butAuthorize.Text = "Twitter連携";
            this.butAuthorize.UseVisualStyleBackColor = true;
            this.butAuthorize.Click += new System.EventHandler(this.butAuthorize_Click);
            // 
            // txtPIN
            // 
            this.txtPIN.Location = new System.Drawing.Point(26, 55);
            this.txtPIN.Name = "txtPIN";
            this.txtPIN.Size = new System.Drawing.Size(173, 19);
            this.txtPIN.TabIndex = 1;
            // 
            // butInputPIN
            // 
            this.butInputPIN.Location = new System.Drawing.Point(205, 53);
            this.butInputPIN.Name = "butInputPIN";
            this.butInputPIN.Size = new System.Drawing.Size(75, 23);
            this.butInputPIN.TabIndex = 2;
            this.butInputPIN.Text = "PIN入力";
            this.butInputPIN.UseVisualStyleBackColor = true;
            this.butInputPIN.Click += new System.EventHandler(this.butInputPIN_Click);
            // 
            // txtTweet
            // 
            this.txtTweet.Location = new System.Drawing.Point(26, 88);
            this.txtTweet.Multiline = true;
            this.txtTweet.Name = "txtTweet";
            this.txtTweet.Size = new System.Drawing.Size(254, 208);
            this.txtTweet.TabIndex = 3;
            // 
            // butTweet
            // 
            this.butTweet.Location = new System.Drawing.Point(205, 315);
            this.butTweet.Name = "butTweet";
            this.butTweet.Size = new System.Drawing.Size(75, 23);
            this.butTweet.TabIndex = 4;
            this.butTweet.Text = "ツイート";
            this.butTweet.UseVisualStyleBackColor = true;
            this.butTweet.Click += new System.EventHandler(this.butTweet_Click);
            // 
            // FormTwitterPost
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(313, 369);
            this.Controls.Add(this.butTweet);
            this.Controls.Add(this.txtTweet);
            this.Controls.Add(this.butInputPIN);
            this.Controls.Add(this.txtPIN);
            this.Controls.Add(this.butAuthorize);
            this.Name = "FormTwitterPost";
            this.Text = "Twitter投稿";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button butAuthorize;
        private System.Windows.Forms.TextBox txtPIN;
        private System.Windows.Forms.Button butInputPIN;
        private System.Windows.Forms.TextBox txtTweet;
        private System.Windows.Forms.Button butTweet;
    }
}

