﻿using System;
using System.Windows.Forms;

namespace MyHousekeepingBook1
{
    public partial class ItemForm : Form
    {
        public ItemForm(CategoryDataSet dsCategory
)
        {
            InitializeComponent();
            categoryDataSet.Merge(dsCategory);
        }

        public ItemForm(CategoryDataSet dsCategory,
    DateTime nowDate,
    string category,
    string item,
    int money,
    string remarks)
        {
            InitializeComponent();
            categoryDataSet.Merge(dsCategory);
            monCalendar.SetDate(nowDate);
            cmbCategory.Text = category;
            txtItem.Text = item;
            mtxtMoney.Text = money.ToString();
            txtRemarks.Text = remarks;
        }

        private void cmbCategory_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}